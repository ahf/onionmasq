#!/bin/sh
#
# Installs Rust cross-compilation toolchains for all supported architectures.
#
set -e
TARGETS="armv7-linux-androideabi aarch64-linux-android i686-linux-android x86_64-linux-android"
rustup install "stable"
rustup target add $TARGETS
rustup default stable

