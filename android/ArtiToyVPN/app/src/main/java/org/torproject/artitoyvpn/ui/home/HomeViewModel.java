package org.torproject.artitoyvpn.ui.home;

import android.content.Context;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import org.torproject.artitoyvpn.R;
import org.torproject.artitoyvpn.vpn.VpnStatusObservable;

public class HomeViewModel extends ViewModel {

    final MutableLiveData<String> stateText;
    final MutableLiveData<String> buttonText;
    final MutableLiveData<Boolean> isButtonEnabled;

    public HomeViewModel() {
        super();
        stateText = new MutableLiveData<>();
        buttonText = new MutableLiveData<>();
        isButtonEnabled = new MutableLiveData<>();
    }

    public LiveData<String> getText() {
        return stateText;
    }

    public LiveData<String> getButtonText() {
        return buttonText;
    }

    public LiveData<Boolean> isButtonEnabled() {
        return isButtonEnabled;
    }


    public void update(VpnStatusObservable.Status status, Context context) {
        switch (status) {
            case STARTING:
                stateText.postValue(context.getString(R.string.state_description_starting));
                buttonText.postValue(context.getString(R.string.stop));
                isButtonEnabled.postValue(false);
                break;
            case RUNNING:
                stateText.postValue(context.getString(R.string.state_description_running));
                buttonText.postValue(context.getString(R.string.stop));
                isButtonEnabled.postValue(true);
                break;
            case STOPPING:
                stateText.postValue(context.getString(R.string.state_description_stopping));
                buttonText.postValue(context.getString(R.string.stop));
                isButtonEnabled.postValue(false);
                break;
            case STOPPED:
                stateText.postValue(context.getString(R.string.state_description_off));
                buttonText.postValue(context.getString(R.string.start));
                isButtonEnabled.postValue(true);
                break;
            case ERROR:
                stateText.postValue(context.getString(R.string.state_description_error));
                buttonText.postValue(context.getString(R.string.start));
                isButtonEnabled.postValue(true);
                break;
        }
    }
}