package org.torproject.artitoyvpn.ui.home;

import android.app.Activity;
import android.content.Intent;
import android.net.VpnService;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import org.torproject.artitoyvpn.MainActivity;
import org.torproject.artitoyvpn.R;
import org.torproject.artitoyvpn.databinding.FragmentHomeBinding;
import org.torproject.artitoyvpn.utils.Utils;
import org.torproject.artitoyvpn.vpn.VpnServiceCommand;
import org.torproject.artitoyvpn.vpn.VpnStatusObservable;
import org.torproject.artitoyvpn.vpn.VpnStatusObservable.Status;

import java.util.Timer;
import java.util.TimerTask;

public class HomeFragment extends Fragment {

    private HomeViewModel homeViewModel;
    private FragmentHomeBinding binding;

    ActivityResultLauncher<Intent> startForResult = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(),
            new ActivityResultCallback<ActivityResult>() {
                @Override
                public void onActivityResult(ActivityResult result) {
                    if (result.getResultCode() == Activity.RESULT_OK) {
                        VpnServiceCommand.startVpn(getContext());
                    } else {
                        VpnStatusObservable.update(Status.ERROR);
                    }
                }
            });

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        homeViewModel =
                new ViewModelProvider(this).get(HomeViewModel.class);

        binding = FragmentHomeBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        final TextView textView = binding.textVpnState;
        homeViewModel.getText().observe(getViewLifecycleOwner(), s -> textView.setText(s));

        final Button vpnButton = binding.buttonMain;
        VpnStatusObservable.getStatus().observe(getViewLifecycleOwner(), status -> homeViewModel.update(status, getContext()));
        homeViewModel.getButtonText().observe(getViewLifecycleOwner(), text -> vpnButton.setText(text));
        homeViewModel.isButtonEnabled().observe(getViewLifecycleOwner(), enabled -> vpnButton.setEnabled(enabled));

        vpnButton.setOnClickListener(view -> {
            Status status = VpnStatusObservable.getStatus().getValue();
            switch (status) {
                case RUNNING:
                    VpnStatusObservable.update(Status.STOPPING);
                    VpnServiceCommand.stopVpn(view.getContext());
                    break;
                case STOPPED:
                case ERROR:
                    VpnStatusObservable.update(Status.STARTING);
                    prepareToStartVPN();
                    break;
                default:
                    break;

            }
        });
        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        homeViewModel.stateText.setValue(getContext().getString(R.string.state_description_off));
        homeViewModel.buttonText.setValue(getContext().getString(R.string.start));
    }

    public void prepareToStartVPN() {
        Intent vpnIntent = null;
        try {
            vpnIntent = VpnService.prepare(getContext().getApplicationContext()); // stops the VPN connection created by another application.
        } catch (NullPointerException npe) {
            VpnStatusObservable.update(VpnStatusObservable.Status.ERROR);
        }
        if (vpnIntent != null) {
            startForResult.launch(vpnIntent);
        } else {
            VpnServiceCommand.startVpn(getContext());
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}