package org.torproject.artitoyvpn.ui.appListing;

import android.graphics.drawable.Drawable;

class DataItem implements Comparable<DataItem> {
    boolean isSelected;
    String appName;
    Drawable appIcon;
    int appUID;
    String packageName;

    public DataItem(String appName, Drawable appIcon, int appUID, String packageName, boolean isSelected) {
        this.isSelected = isSelected;
        this.appName = appName;
        this.appIcon = appIcon;
        this.appUID = appUID;
        this.packageName = packageName;
    }

    public DataItem(boolean isSelected) {
        this.isSelected = isSelected;
        this.appName = "";
        this.appIcon = null;
        this.appUID = 0;
        this.packageName = null;

    }

    static DataItem createAppItem(String appName, Drawable appIcon, int appUID, String packageName, boolean isSelected) {
        return new DataItem(appName, appIcon, appUID, packageName, isSelected);
    }

    static DataItem createHeaderItem(boolean isSelected) {
        return new DataItem(isSelected);
    }

    //Shorting is done over name so this comparison is over the app name.
    @Override
    public int compareTo(DataItem o) {
        return this.appName.compareTo(o.appName);
    }
}



