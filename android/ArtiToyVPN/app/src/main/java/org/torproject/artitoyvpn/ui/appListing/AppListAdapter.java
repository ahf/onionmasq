package org.torproject.artitoyvpn.ui.appListing;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import org.torproject.artitoyvpn.databinding.ItemAppsBinding;
import org.torproject.artitoyvpn.databinding.ItemSelectAllAppsBinding;

import java.util.ArrayList;
import java.util.List;

class AppListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final List<DataItem> items = new ArrayList<>();
    private final AppListingViewModel viewModel;
    private final int rowTypeHeader = 0;
    private final int rowTypeAppItem = 1;

    public AppListAdapter(AppListingViewModel viewModel) {
        this.viewModel = viewModel;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == rowTypeHeader) {
            return new HeaderViewHolder(ItemSelectAllAppsBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false));

        } else {
            return new AppListViewHolder(ItemAppsBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof AppListViewHolder) {
            ((AppListViewHolder) holder).setAppDetail(items.get(position), position);

        } else if (holder instanceof HeaderViewHolder) {
            ((HeaderViewHolder) holder).setSelectAllBox(items.get(position));

        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public int getItemViewType(int position) {
        return items.get(position).packageName == null ? rowTypeHeader : rowTypeAppItem;
    }

    public void setData(List<DataItem> data) {
        items.clear();
        items.addAll(data);
        notifyDataSetChanged();
    }

    class AppListViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private final ItemAppsBinding mBinding;
        private int pos = -1;

        public AppListViewHolder(@NonNull ItemAppsBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;
        }

        public void setAppDetail(DataItem dataItem, int pos) {
            this.pos = pos;
            mBinding.imageView.setImageDrawable(dataItem.appIcon);
            mBinding.textView.setText(dataItem.appName);
            mBinding.textView3.setText(dataItem.appUID + " | " + dataItem.packageName);
            mBinding.checkBox.setChecked(dataItem.isSelected);
            mBinding.checkBox.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            AppListAdapter.this.viewModel.rowClicked(pos, mBinding.checkBox.isChecked());
        }
    }

    class HeaderViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private final ItemSelectAllAppsBinding mBinding;

        public HeaderViewHolder(@NonNull ItemSelectAllAppsBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;
        }

        public void setSelectAllBox(DataItem dataItem) {
            mBinding.checkBox2.setChecked(dataItem.isSelected);
            mBinding.checkBox2.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            AppListAdapter.this.viewModel.selectAllRowClicked(mBinding.checkBox2.isChecked());
        }
    }

}

