#include <errno.h>
#include <grp.h>
#include <stdlib.h>
#include <string.h>

// This function is missing from bionic and required to implement some of the functions here.
// I don't believe having two implementation of pthread at the same time is advisable, so instead
// we just do nothing. This means some section which don't want to get cancelled can now be.
// It sounds bad, but libonionmasq_mobile.so does not search for the symbol "pthread_cancel", so
// I believe this is fine. One thing that could prove me wrong is if Android JVM decides to call
// "pthread_cancel" itself.
int pthread_setcancelstate(int state, int *oldstate) {
        return 0;
};

#define PTHREAD_CANCEL_DISABLE 1

// files containing code other things depend on. 
#include "include/features.h"
#include "src/passwd/getgrent_a.c"
#include "src/passwd/getgr_a.c"
#include "src/passwd/nscd_query.c"
#include "src/network/netlink.c"

// files containing the functions we are interested in
#include "src/misc/lockf.c"
#include "src/time/clock_getcpuclockid.c"
#include "src/passwd/getgr_r.c"
#include "src/network/getifaddrs.c"
