#![allow(non_snake_case)]
use jni::objects::{GlobalRef, JString};
use jni::objects::{JClass, JObject, JValue};
use jni::{JNIEnv, JavaVM};
use once_cell::sync::OnceCell;
use onion_tunnel::OnionTunnel;
use std::fmt::{Debug, Formatter, Result};
use std::io;
use std::io::ErrorKind;
use std::net::{IpAddr, SocketAddr};
use std::os::unix::io::RawFd;
use tokio::sync::broadcast;
use tracing::{debug, error, info};
use tracing_subscriber::fmt::Subscriber;
use tracing_subscriber::layer::SubscriberExt;
use tracing_subscriber::util::SubscriberInitExt;

static GLOBAL_JVM_PROVIDER: OnceCell<JvmProvider> = OnceCell::new();
static PROTECT_REF: OnceCell<GlobalRef> = OnceCell::new();
const HOLDER_FIELD_NAME: &str = "exitPointer";

pub struct JvmProvider {
    jvm: JavaVM,
}

impl Debug for JvmProvider {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result {
        f.debug_struct("JvmProvider")
            .field("jvm", &"JavaVM")
            .finish()
    }
}

impl JvmProvider {
    pub fn getJavaVM() -> &'static JavaVM {
        let provider = GLOBAL_JVM_PROVIDER
            .get()
            .expect("JvmProvider is not initialized");
        &provider.jvm
    }

    pub fn new(jvm: JavaVM) -> Self {
        let jvm = jvm;
        Self { jvm }
    }
}

#[no_mangle]
pub extern "C" fn Java_org_torproject_OnionMasq_runProxy(
    env: JNIEnv,
    _: JClass,
    fd: i32,
    interface_name: JString,
    cache_dir: JString,
    data_dir: JString,
    exit_pointer: JObject,
) {
    panic_handling::capture_unwind!(env, {
        let apiVersion = getAndroidAPI();
        debug!("Onionmasq_runProxy on Android API {}", apiVersion);
        let rt = tokio::runtime::Runtime::new().expect("failed to create tokio runtime");
        let cache_dir = env.get_string(cache_dir).expect("cache_dir is invalid");
        let cache_dir = cache_dir.to_string_lossy();
        let data_dir = env.get_string(data_dir).expect("data_dir is invalid");
        let data_dir = data_dir.to_string_lossy();

        // the broadcast channel created here is stored on java and is accessible in different methods without storing it globally.
        let (android_exit_sender, android_exit_receiver) = broadcast::channel(2);

        let _ = match env.set_rust_field(exit_pointer, HOLDER_FIELD_NAME, android_exit_sender) {
            Ok(()) => {
                //debug log
                debug!("exit pointer stored in java");
            }
            Err(error) => {
                debug!("Error in storing exit pointer in java: {:?}", error);
            }
        };

        let config = onion_tunnel::TorClientConfigBuilder::from_directories(
            format!("{}/arti-data", data_dir),
            format!("{}/arti-cache", cache_dir),
        )
        .build()
        .unwrap();
        rt.block_on(async move {
            debug!("creating onion_tunnel...");

            let mut onion_tunnel: OnionTunnel;
            match OnionTunnel::create_with_fd(
                protect,
                &env.get_string(interface_name)
                    .expect("interface_name is invalid")
                    .to_string_lossy(),
                fd,
                config,
            )
            .await
            {
                Ok(v) => {
                    debug!("successfully created tun interface");
                    onion_tunnel = v;
                }
                Err(e) => {
                    debug!("couldn't start onionmasq proxy ({:?})", e);
                    return;
                }
            }
            debug!("starting onionmasq...");
            onion_tunnel
                .start_with_receiver(Some(android_exit_receiver))
                .await;
            debug!("stopped onionmasq...");
        });
    })
}

#[no_mangle]
pub extern "C" fn Java_org_torproject_OnionMasq_closeProxy(
    env: JNIEnv,
    _: JClass,
    exit_pointer: JObject,
) {
    panic_handling::capture_unwind!(env, {
        debug!("closing proxy...");

        // Retriving sender to trigger close
        match env.take_rust_field::<JObject, &str, tokio::sync::broadcast::Sender<i32>>(
            exit_pointer,
            HOLDER_FIELD_NAME,
        ) {
            Ok(android_exit_sender) => match android_exit_sender.send(1) {
                Ok(_) => {
                    debug!("send exit OK");
                }
                Err(error) => {
                    debug!("send exit FAILED");
                    debug!("{:?}", error);
                }
            },
            Err(error) => debug!("take_rust_field error {:?}", error),
        }
    })
}

#[no_mangle]
pub extern "C" fn Java_org_torproject_OnionMasq_init(env: JNIEnv, _: JClass) {
    panic_handling::capture_unwind!(env, {
        if GLOBAL_JVM_PROVIDER.get().is_none() {
            panic_handling::register_panic_hook();
            Subscriber::builder()
                .with_env_filter("info,onionmasq_mobile=debug,onion_tunnel=trace,arti_client=debug,tor_chanmgr=debug,tor_proto=debug,smoltcp=trace")
                .finish()
                .with(tracing_android::layer("onionmasq").unwrap())
                .init(); // this must be called only once, otherwise your app will probably crash
            info!("Hello from Rust!");
            // `JNIEnv` cannot be sent across thread boundaries. To be able to use JNI
            // functions in other threads, we must first obtain the `JavaVM` interface
            // which, unlike `JNIEnv` is `Send`.
            let jvm = env.get_java_vm().unwrap();
            GLOBAL_JVM_PROVIDER.set(JvmProvider::new(jvm)).unwrap();
            info!("Getting reference to \"org.torproject.OnionMasq\" class...");
            let class = env
                .find_class("org/torproject/OnionMasq")
                .expect("Failed to load the target class");
            let reference = env
                .new_global_ref(class)
                .expect("Failed to make global ref");
            let _ = PROTECT_REF.set(reference);
            info!("Stored class reference!");
        } else {
            error!("init is only allowed to be called once")
        }
    })
}

pub fn getAndroidAPI() -> i32 {
    let local_jvm = JvmProvider::getJavaVM();
    let guard = local_jvm.attach_current_thread().unwrap();
    let class = guard
        .find_class("org/torproject/OnionMasq")
        .expect("Failed to load the target class");
    match guard.call_static_method(class, "getAndroidAPI", "()I", &[]) {
        Ok(result) => result.i().unwrap() as i32,
        _Error => -1,
    }
}

pub fn protect(socket: RawFd) -> std::io::Result<()> {
    info!("protect(): getting ref");
    let pref = PROTECT_REF.get().expect("no protect ref!");
    info!("protect(): getting VM");
    let local_jvm = JvmProvider::getJavaVM();
    let guard = local_jvm.attach_current_thread().unwrap();
    info!("protect(): calling method");

    match guard.call_static_method(pref, "protect", "(I)Z", &[JValue::Int(socket)]) {
        Ok(result) => match result.z() {
            Ok(true) => Ok(()),
            Ok(false) => Err(io::Error::new(
                ErrorKind::Other,
                format!("JNI protect() returned error"),
            )),
            Err(_) => Err(io::Error::new(
                ErrorKind::Other,
                format!(
                    "JNI protect() failed to be a boolean: was {}",
                    result.type_name()
                ),
            )),
        },
        Err(e) => Err(io::Error::new(
            ErrorKind::Other,
            format!("failed to call JNI protect(): {:?}", e),
        )),
    }
}

pub fn getConnectionOwnerUid(
    protocol: i32,
    source_address: IpAddr,
    source_port: i32,
    destination_address: IpAddr,
    destination_port: i32,
) -> i32 {
    // https://developer.android.com/reference/android/net/ConnectivityManager#getConnectionOwnerUid(int,%20java.net.InetSocketAddress,%20java.net.InetSocketAddress)
    if getAndroidAPI() < 29 {
        return get_connection_owner_from_procfs(
            protocol,
            SocketAddr::new(source_address, source_port as u16),
            SocketAddr::new(destination_address, destination_port as u16),
        )
        .unwrap_or(-1);
    }

    let local_jvm = JvmProvider::getJavaVM();

    let guard = local_jvm.attach_current_thread().unwrap();
    let class = guard
        .find_class("org/torproject/OnionMasq")
        .expect("Failed to load the target class");

    let source_ip_bytes = match source_address {
        IpAddr::V4(source_address) => source_address.octets().to_vec(),
        IpAddr::V6(source_address) => source_address.octets().to_vec(),
    };
    let destination_ip_bytes = match destination_address {
        IpAddr::V4(destination_address) => destination_address.octets().to_vec(),
        IpAddr::V6(destination_address) => destination_address.octets().to_vec(),
    };

    let source_address_jbyte_array = guard
        .byte_array_from_slice(source_ip_bytes.as_slice())
        .unwrap();
    let destination_address_jbyte_array = guard
        .byte_array_from_slice(destination_ip_bytes.as_slice())
        .unwrap();

    match guard.call_static_method(
        class,
        "getConnectionOwnerUid",
        "(I[BI[BI)I",
        &[
            JValue::from(protocol),
            JValue::from(source_address_jbyte_array),
            JValue::from(source_port),
            JValue::from(destination_address_jbyte_array),
            JValue::from(destination_port),
        ],
    ) {
        Ok(result) => result.i().unwrap() as i32,
        _Error => -1,
    }
}

pub fn get_connection_owner_from_procfs(
    protocol: i32,
    source: SocketAddr,
    destination: SocketAddr,
) -> procfs::ProcResult<i32> {
    Ok(match protocol {
        6 => {
            let entries = if source.is_ipv4() {
                procfs::net::tcp()?
            } else {
                procfs::net::tcp6()?
            };
            entries
                .iter()
                .filter(|entry| {
                    entry.local_address == source && entry.remote_address == destination
                })
                .map(|entry| entry.uid as i32)
                .next()
                .unwrap_or(-1)
        }
        17 => {
            let entries = if source.is_ipv4() {
                procfs::net::udp()?
            } else {
                procfs::net::udp6()?
            };
            entries
                .iter()
                .filter(|entry| {
                    entry.local_address == source && entry.remote_address == destination
                })
                .map(|entry| entry.uid as i32)
                .next()
                .unwrap_or(-1)
        }
        _ => -1,
    })
}

mod panic_handling {
    use lazy_static::lazy_static;
    use std::sync::{Arc, Mutex};

    lazy_static! {
        pub(crate) static ref BACKTRACE: Arc<Mutex<Option<String>>> = Arc::new(Mutex::new(None));
    }

    // on panic, save the backtrace here (as string)
    macro_rules! capture_unwind {
        ($env:expr, $code:expr) => {
            match std::panic::catch_unwind(|| $code) {
                Ok(v) => v,
                Err(e) => {
                    let panic_information = match e.downcast::<String>() {
                        Ok(v) => *v,
                        Err(e) => match e.downcast::<&str>() {
                            Ok(v) => v.to_string(),
                            _ => "Unknown Source of Error".to_owned()
                        }
                    };
                    let message = format!("{}\nBacktrace:\n{}", panic_information, panic_handling::BACKTRACE.lock().unwrap().take().unwrap_or("<Backtrace not found>".to_string()));
                    error!("{message}");
                    // using unwrap here might cause issues (abort), but there isn't really a better thing to do
                    $env.throw_new("java/lang/Exception", message).unwrap();
                }
            }
        }
    }
    pub(crate) use capture_unwind;

    fn panic_hook(_: &std::panic::PanicInfo) {
        *BACKTRACE.lock().unwrap() = Some(format!("{:?}", backtrace::Backtrace::new()));
    }

    pub(crate) fn register_panic_hook() {
        std::panic::set_hook(Box::new(panic_hook));
    }
}
