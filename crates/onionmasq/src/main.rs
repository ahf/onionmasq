use onion_tunnel::OnionTunnel;
use std::os::unix::io::RawFd;
use tracing_subscriber::FmtSubscriber;

fn protect_dummy(_unused: RawFd) -> std::io::Result<()> {
    Ok(())
}

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    FmtSubscriber::builder()
        .with_env_filter("info,smoltcp=trace,onionmasq_device_testing=trace,onion_tunnel=trace,arti_client=debug,tor_chanmgr=debug,tor_proto=debug")
        .init();

    let mut onion_tunnel = OnionTunnel::new(protect_dummy, "onion0", Default::default()).await;

    tokio::select! {
        _ = onion_tunnel.start() => (),
    };

    Ok(())
}
