// Copyright (c) 2021, The Tor Project, Inc.
// See LICENSE for licensing information.

use bytes::Bytes;

use dns_message_parser::Dns;

use smoltcp::iface::{Interface, SocketHandle};
use smoltcp::phy::Device;
use smoltcp::socket::{UdpPacketMetadata, UdpSocket, UdpSocketBuffer};
use smoltcp::wire::IpAddress;

pub struct DnsManager {
    sockets: Vec<SocketHandle>,
}

impl DnsManager {
    pub fn new() -> DnsManager {
        DnsManager {
            sockets: Vec::new(),
        }
    }

    pub fn register_udp_listener<DeviceT: for<'d> Device<'d>>(
        &mut self,
        address: IpAddress,
        port: u16,
        interface: &mut Interface<DeviceT>,
    ) {
        log::info!(
            "Registering new DNS-over-UDP listener on {}:{}",
            address,
            port
        );

        let rx_buffer = UdpSocketBuffer::new(vec![UdpPacketMetadata::EMPTY], vec![0; 4096]);
        let tx_buffer = UdpSocketBuffer::new(vec![UdpPacketMetadata::EMPTY], vec![0; 4096]);
        let socket = UdpSocket::new(rx_buffer, tx_buffer);

        let handle = interface.add_socket(socket);
        let socket = interface.get_socket::<UdpSocket>(handle);
        socket.bind((address, port)).unwrap();

        self.sockets.push(handle);
    }

    pub fn process_sockets<DeviceT: for<'d> Device<'d>>(&self, interface: &mut Interface<DeviceT>) {
        for handle in self.sockets.iter() {
            self.process_socket(*handle, interface);
        }
    }

    fn process_socket<DeviceT: for<'d> Device<'d>>(
        &self,
        handle: SocketHandle,
        interface: &mut Interface<DeviceT>,
    ) {
        let socket = interface.get_socket::<UdpSocket>(handle);

        match socket.recv() {
            Ok((data, endpoint)) => {
                log::trace!("DNS packet from {}: {:?}", endpoint, data);

                match Dns::decode(Bytes::copy_from_slice(&data[..])) {
                    Ok(question) => {
                        log::trace!("DNS question: {:?}", question);

                        // socket.send_slice(question.as_bytes(), endpoint).unwrap();
                    }
                    Err(error) => {
                        log::error!("Unable to parse DNS request: {}", error);
                    }
                }
            }

            Err(_) => {
                log::error!("Read error when reading from DNS listener");
            }
        }
    }
}
