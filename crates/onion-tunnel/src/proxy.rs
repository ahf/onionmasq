use arti_client::DangerouslyIntoTorAddr;
use arti_client::TorAddr;
use arti_client::TorClient;
use std::future::Future;
use std::io;
use std::io::{Error, ErrorKind};
use std::net::IpAddr;
use std::pin::Pin;
use std::task::{Context, Poll};
use tokio::io::{AsyncRead, AsyncWrite, ReadBuf};
use tracing::info;

use crate::dns::LockedDnsCookies;
use crate::runtime::OnionTunnelArtiRuntime;
use crate::socket::TcpSocket;

struct InteractiveCopier<R, W> {
    reader: R,
    writer: W,
    buffer: [u8; 1024],
    start: usize,
    end: usize,
    flushing: bool,
}

impl<R, W> InteractiveCopier<R, W>
where
    R: AsyncRead + Unpin,
    W: AsyncWrite + Unpin,
{
    fn new(reader: R, writer: W) -> Self {
        Self {
            reader,
            writer,
            buffer: [0; 1024],
            start: 0,
            end: 0,
            flushing: false,
        }
    }
}

impl<R, W> Future for InteractiveCopier<R, W>
where
    R: AsyncRead + Unpin,
    W: AsyncWrite + Unpin,
{
    type Output = io::Result<()>;

    fn poll(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output> {
        let this = Pin::into_inner(self);
        loop {
            if this.flushing {
                // When the writer's finished flushing, we're done too (reader got closed).
                return Pin::new(&mut this.writer).poll_flush(cx);
            }
            // First, check if we have any data to write out.
            if this.end != 0 {
                let bytes_to_write = this.end - this.start;
                assert_ne!(bytes_to_write, 0);

                match Pin::new(&mut this.writer).poll_write(cx, &this.buffer[this.start..this.end])
                {
                    // Writer closed.
                    Poll::Ready(Ok(0)) => return Poll::Ready(Ok(())),
                    Poll::Ready(Ok(v)) => {
                        if v == bytes_to_write {
                            // We've written the whole buffer, so clear it out.
                            this.end = 0;
                            this.start = 0;
                        } else {
                            // Advance the start pointer to the start of the bit
                            // we haven't written yet, and try to write that bit.
                            this.start = v;
                            continue;
                        }
                    }
                    Poll::Ready(Err(e)) => return Poll::Ready(Err(e)),
                    Poll::Pending => return Poll::Pending,
                }
            }
            // Once we've written everything out, try and read.
            let mut read_buf = ReadBuf::new(&mut this.buffer);
            match Pin::new(&mut this.reader).poll_read(cx, &mut read_buf) {
                Poll::Ready(Ok(())) => {
                    let bytes_read = read_buf.filled().len();
                    if bytes_read == 0 {
                        // Reader's closed, so just flush out all the written data.
                        this.flushing = true;
                        continue;
                    }
                    this.end = bytes_read;
                    continue;
                }
                Poll::Ready(Err(e)) => return Poll::Ready(Err(e)),
                Poll::Pending => {
                    // Nothing to read, so let's flush.
                    if let Poll::Ready(Err(e)) = Pin::new(&mut this.writer).poll_flush(cx) {
                        return Poll::Ready(Err(e));
                    }
                    return Poll::Pending;
                }
            }
        }
    }
}

pub struct ArtiProxy {
    socket: TcpSocket,
    arti: TorClient<OnionTunnelArtiRuntime>,
    cookies: LockedDnsCookies,
}

impl ArtiProxy {
    pub fn new(
        socket: TcpSocket,
        arti: TorClient<OnionTunnelArtiRuntime>,
        cookies: LockedDnsCookies,
    ) -> Self {
        Self {
            socket,
            arti,
            cookies,
        }
    }

    pub async fn start(&mut self) -> io::Result<()> {
        let dest = self.socket.dest();
        let tor_addr;

        if let Some(hostname) = self.cookies.lock().unwrap().get(&dest.0) {
            tor_addr = TorAddr::from((hostname.clone(), dest.1));
        } else {
            let ip_addr: IpAddr = dest.0.into();
            tor_addr = (ip_addr, dest.1).into_tor_addr_dangerously();
        }

        let tor_addr = match tor_addr {
            Ok(v) => v,
            Err(e) => {
                return Err(Error::new(
                    ErrorKind::Other,
                    format!("failed to resolve {:?} into a TorAddr: {:?}", dest, e),
                ));
            }
        };

        info!("Initiating Arti connection to {:?}", tor_addr);
        let arti_stream = match self.arti.connect(tor_addr.clone()).await {
            Ok(v) => v,
            Err(e) => {
                return Err(Error::new(
                    ErrorKind::Other,
                    format!("failed to connect to {:?} via Arti: {}", tor_addr, e),
                ));
            }
        };
        info!("Got Arti stream for {:?}", tor_addr);

        let (arti_r, arti_w) = arti_stream.split();
        let arti_to_tun = InteractiveCopier::new(arti_r, self.socket.clone());
        let tun_to_arti = InteractiveCopier::new(self.socket.clone(), arti_w);

        loop {
            tokio::select! {
                result = arti_to_tun => break result,
                result = tun_to_arti => break result,
            };
        }
    }
}
