use async_trait::async_trait;
use std::io;
use std::io::{IoSlice, IoSliceMut, Result as IoResult};
use std::net::SocketAddr;
use std::os::unix::io::{AsRawFd, RawFd};
use std::pin::Pin;
use std::sync::Arc;
use std::task::{Context, Poll};

use futures::{AsyncRead, AsyncWrite, Stream};
use tokio::net::{TcpSocket, TcpStream};
use tokio_util::compat::TokioAsyncReadCompatExt;
use tor_rtcompat::tokio::TokioNativeTlsRuntime as ArtiRuntime;
use tor_rtcompat::{CompoundRuntime, TcpListener, TcpProvider};

use tracing::{info, warn};

use crate::netlink::Netlink;

pub type OnionTunnelArtiRuntime =
    CompoundRuntime<ArtiRuntime, ArtiRuntime, OnionTunnelArtiTcpProvider, ArtiRuntime, ArtiRuntime>;

pub async fn make_runtime<F>(protect: F) -> OnionTunnelArtiRuntime
where
    F: Fn(RawFd) -> io::Result<()> + Send + Sync + 'static,
{
    let rt = ArtiRuntime::current().expect("Unable to get runtime");
    let tcp_provider = OnionTunnelArtiTcpProvider {
        netlink: Netlink::new().await,
        protect: Arc::new(protect),
    };
    CompoundRuntime::new(rt.clone(), rt.clone(), tcp_provider, rt.clone(), rt)
}

#[derive(Clone)]
pub struct OnionTunnelArtiTcpProvider {
    netlink: Netlink,
    /// Protect function.
    protect: Arc<dyn Fn(RawFd) -> io::Result<()> + Send + Sync>,
}

pub struct OnionTcpStream {
    inner: TcpStream,
}

pub struct Unimplemented;

#[async_trait]
impl TcpProvider for OnionTunnelArtiTcpProvider {
    type TcpStream = OnionTcpStream;
    type TcpListener = Unimplemented;

    async fn connect(&self, addr: &SocketAddr) -> IoResult<Self::TcpStream> {
        info!("New Arti TCP connection to {}", addr);
        let _ = self.netlink.add_passthrough_route(&addr.ip().into()).await;
        let socket = if addr.is_ipv4() {
            TcpSocket::new_v4()?
        } else {
            TcpSocket::new_v6()?
        };
        let fd = socket.as_raw_fd();
        info!("protecting fd {} for socket to {}", fd, addr);
        (self.protect)(fd)?;
        let inner = match socket.connect(*addr).await {
            Ok(x) => {
                info!("Arti connection to {} successful", addr);
                x
            }
            Err(e) => {
                warn!("Arti failed to connect to {}: {}", addr, e);
                return Err(e);
            }
        };
        Ok(OnionTcpStream { inner })
    }

    async fn listen(&self, _: &SocketAddr) -> IoResult<Self::TcpListener> {
        panic!("attempted to call OnionTunnelTcpProvider::listen()!")
    }
}

impl futures::io::AsyncRead for OnionTcpStream {
    fn poll_read(
        self: Pin<&mut Self>,
        cx: &mut Context<'_>,
        buf: &mut [u8],
    ) -> Poll<IoResult<usize>> {
        let this = Pin::into_inner(self);
        let this = &mut this.inner;
        AsyncRead::poll_read(Pin::new(&mut this.compat()), cx, buf)
    }

    fn poll_read_vectored(
        self: Pin<&mut Self>,
        cx: &mut Context<'_>,
        bufs: &mut [IoSliceMut<'_>],
    ) -> Poll<IoResult<usize>> {
        let this = Pin::into_inner(self);
        let this = &mut this.inner;
        AsyncRead::poll_read_vectored(Pin::new(&mut this.compat()), cx, bufs)
    }
}

impl futures::io::AsyncWrite for OnionTcpStream {
    fn poll_write(self: Pin<&mut Self>, cx: &mut Context<'_>, buf: &[u8]) -> Poll<IoResult<usize>> {
        let this = Pin::into_inner(self);
        let this = &mut this.inner;
        AsyncWrite::poll_write(Pin::new(&mut this.compat()), cx, buf)
    }

    fn poll_write_vectored(
        self: Pin<&mut Self>,
        cx: &mut Context<'_>,
        bufs: &[IoSlice<'_>],
    ) -> Poll<IoResult<usize>> {
        let this = Pin::into_inner(self);
        let this = &mut this.inner;
        AsyncWrite::poll_write_vectored(Pin::new(&mut this.compat()), cx, bufs)
    }

    fn poll_flush(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<IoResult<()>> {
        let this = Pin::into_inner(self);
        let this = &mut this.inner;
        AsyncWrite::poll_flush(Pin::new(&mut this.compat()), cx)
    }

    fn poll_close(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<IoResult<()>> {
        let this = Pin::into_inner(self);
        let this = &mut this.inner;
        AsyncWrite::poll_close(Pin::new(&mut this.compat()), cx)
    }
}
#[async_trait]
impl TcpListener for Unimplemented {
    type TcpStream = OnionTcpStream;
    type Incoming = Unimplemented;

    async fn accept(&self) -> IoResult<(Self::TcpStream, SocketAddr)> {
        unimplemented!()
    }

    fn incoming(self) -> Self::Incoming {
        unimplemented!()
    }

    fn local_addr(&self) -> IoResult<SocketAddr> {
        unimplemented!()
    }
}

impl Stream for Unimplemented {
    type Item = IoResult<(OnionTcpStream, SocketAddr)>;

    fn poll_next(self: Pin<&mut Self>, _: &mut Context<'_>) -> Poll<Option<Self::Item>> {
        unimplemented!()
    }
}
