use std::net::{IpAddr, Ipv4Addr};

use futures::stream::TryStreamExt;

use ipnetwork::IpNetwork;
use netlink_packet_route::{RTN_UNICAST, RT_SCOPE_UNIVERSE};
use rtnetlink::{new_connection, Error, Handle, IpVersion};

/*
async fn get_link_index_by_name(handle: &Handle, name: &String) -> Result<u32, Error> {
    let mut links = handle.link().get().match_name(name.clone()).execute();
    if let Some(msg) = links.try_next().await? {
        Ok(msg.header.index)
    } else {
        Err(Error::RequestFailed)
    }
}
*/

async fn get_default_gateway4(handle: &Handle) -> Result<Ipv4Addr, Error> {
    let mut routes = handle.route().get(IpVersion::V4).execute();
    while let Some(msg) = routes.try_next().await? {
        if msg.header.destination_prefix_length == 0
            && msg.header.scope == RT_SCOPE_UNIVERSE
            && msg.header.kind == RTN_UNICAST
        {
            match msg.gateway().unwrap() {
                IpAddr::V4(v4) => return Ok(v4),
                _ => (),
            }
        }
    }
    Err(Error::RequestFailed)
}

#[derive(Clone)]
pub struct Netlink {
    gw4_addr: Ipv4Addr,
    handle: Handle,
}

impl Netlink {
    // XXX: Handle errors.
    pub async fn new() -> Self {
        let (conn, handle, _) = new_connection().unwrap();
        tokio::spawn(conn);

        Self {
            gw4_addr: get_default_gateway4(&handle).await.unwrap(),
            handle,
        }
    }

    pub async fn add_passthrough_route(&self, dest: &IpNetwork) -> Result<(), Error> {
        match dest {
            IpNetwork::V4(v4) => {
                self.handle
                    .route()
                    .add()
                    .v4()
                    .destination_prefix(v4.ip(), v4.prefix())
                    .gateway(self.gw4_addr.into())
                    .execute()
                    .await?;
            }
            IpNetwork::V6(_v6) => (),
        }
        Ok(())
    }
}
