mod device;
mod dns;
mod netlink;
mod parser;
mod proxy;
mod runtime;
mod socket;
mod tuntap;

use arti_client::TorClient;
use device::VirtualDevice;
use dns::{DnsManager, LockedDnsCookies};
use futures::future::Either;
use futures::stream::StreamExt;
use onionmasq_device_testing::{AsyncDevice, PollableAsyncDevice};
use proxy::ArtiProxy;
use runtime::OnionTunnelArtiRuntime;
use smoltcp::{
    iface::{Interface, InterfaceBuilder, Routes},
    phy::Medium,
    wire::{IpAddress, IpCidr, Ipv4Address},
};
use std::os::unix::io::RawFd;
use std::{
    collections::BTreeMap,
    io,
    io::Error,
    sync::{Arc, Mutex},
};
use tokio::sync::broadcast::Receiver;
use tokio::task;
use tracing::{debug, error, info, warn};

use crate::tuntap::AsyncTunTapInterface;
use crate::{parser::Parser, socket::TcpSocket};

pub use arti_client::{config::TorClientConfigBuilder, TorClientConfig};

const ASYNC_DEVICE_BUFFER_SIZE: usize = 1500 * 4;

pub type IFace =
    Arc<Mutex<Interface<'static, VirtualDevice<AsyncDevice<ASYNC_DEVICE_BUFFER_SIZE>>>>>;

pub struct OnionTunnel {
    iface_name: String,
    iface: IFace,
    pollable_adev: PollableAsyncDevice<AsyncTunTapInterface, ASYNC_DEVICE_BUFFER_SIZE>,
    arti: TorClient<OnionTunnelArtiRuntime>,
    dns_cookies: Option<LockedDnsCookies>,
}

impl OnionTunnel {
    async fn create_inner<F>(
        protect: F,
        iface_name: &str,
        device: AsyncTunTapInterface,
        arti_config: TorClientConfig,
    ) -> Result<Self, Error>
    where
        F: Fn(RawFd) -> io::Result<()> + Send + Sync + 'static,
    {
        // FIXME(eta): MTU?
        let (adev, pollable_adev) = AsyncDevice::new(device, Medium::Ip, 1500);

        let rt = runtime::make_runtime(protect).await;
        let arti_builder = TorClient::with_runtime(rt).config(arti_config);
        let arti = arti_builder.create_unbootstrapped().unwrap();

        // Create our iface.
        let mut routes = Routes::new(BTreeMap::new());
        routes
            .add_default_ipv4_route(Ipv4Address::new(0, 0, 0, 1))
            .expect("Unable to add default IPv4 route");

        let iface_builder = InterfaceBuilder::new(VirtualDevice::new(adev), Vec::new())
            .ip_addrs([IpCidr::new(IpAddress::v4(0, 0, 0, 1), 0)])
            .routes(routes)
            .any_ip(true);

        let iface = iface_builder.finalize();
        let iface = Arc::new(Mutex::new(iface));

        Ok(Self {
            arti,
            iface_name: iface_name.to_string(),
            iface,
            pollable_adev,
            dns_cookies: None,
        })
    }

    pub async fn create_with_fd<F>(
        protect: F,
        iface_name: &str,
        fd: i32,
        arti_config: TorClientConfig,
    ) -> Result<Self, Error>
    where
        F: Fn(RawFd) -> io::Result<()> + Send + Sync + 'static,
    {
        debug!("OnionTunnel create_with_fd {}", &iface_name);
        let tuntap = match AsyncTunTapInterface::new_from_fd(fd) {
            Ok(v) => {
                debug!("successfully created tun interface");
                v
            }
            Err(e) => {
                warn!("couldn't create tun interface: {}", e);
                return Err(e);
            }
        };

        Self::create_inner(protect, iface_name, tuntap, arti_config).await
    }

    pub async fn new<F>(protect: F, iface_name: &str, arti_config: TorClientConfig) -> Self
    where
        F: Fn(RawFd) -> io::Result<()> + Send + Sync + 'static,
    {
        let tun =
            AsyncTunTapInterface::new(iface_name, Medium::Ip).expect("Unable to create TUN iface");

        Self::create_inner(protect, iface_name, tun, arti_config)
            .await
            .expect("create failed")
    }

    fn iface_poll(&self, timestamp: smoltcp::time::Instant) {
        match self.iface.lock().unwrap().poll(timestamp) {
            Ok(_) | Err(smoltcp::Error::Exhausted) => {}
            Err(error) => {
                debug!("Poll error: {}", error)
            }
        }
    }

    fn proxy(&mut self, mut socket: TcpSocket) -> tokio::task::JoinHandle<()> {
        info!(
            "New proxied connection to {}:{}",
            socket.dest().0,
            socket.dest().1
        );
        let mut proxy = ArtiProxy::new(
            socket.clone(),
            self.arti.clone(),
            self.dns_cookies.as_ref().unwrap().clone(),
        );
        tokio::spawn(async move {
            match proxy.start().await {
                Ok(_) => {
                    info!(
                        "Proxied connection to {}:{} closed.",
                        socket.dest().0,
                        socket.dest().1
                    );
                    socket.with(|s| s.close())
                }
                Err(e) => {
                    warn!(
                        "Proxied connection to {}:{} failed: {}",
                        socket.dest().0,
                        socket.dest().1,
                        e
                    );
                    socket.with(|s| s.abort());
                }
            }
            let _ = socket
                .iface
                .lock()
                .unwrap()
                .poll(smoltcp::time::Instant::now());
        })
    }

    fn dns(&mut self) -> tokio::task::JoinHandle<()> {
        // Spawn the DNS manager service.
        let mut dns_manager = DnsManager::new(self.iface.clone());
        self.dns_cookies = Some(dns_manager.cookies());
        tokio::spawn(async move { dns_manager.start().await })
    }

    pub async fn start(&mut self) {
        self.start_with_receiver(Option::None).await
    }

    pub async fn start_with_receiver(&mut self, mut exit_receiver: Option<Receiver<i32>>) {
        info!("Starting onion tunnel on TUN iface {}", self.iface_name);
        let tc = self.arti.clone();
        tokio::spawn(async move {
            info!("Bootstrapping Arti...");
            match tc.bootstrap().await {
                Ok(_) => info!("Bootstrapping complete."),
                Err(e) => error!("Failed to bootstrap: {}", e),
            }
        });

        // Spawn the DNS manager service.
        let dns_join_handle = self.dns();

        loop {
            let timestamp = smoltcp::time::Instant::now();

            // The first poll we queue packets in the receive queue.
            self.iface_poll(timestamp);

            let mut packets = self.iface.lock().unwrap().device_mut().take_recv_queue();

            let mut proxy_handle: Vec<task::JoinHandle<()>> = Vec::new();
            // Handle incoming packet. Drain packets as we process them.
            while let Some(packet) = packets.pop_front() {
                if let Some(tcp_socket) = Parser::parse(packet.clone()).take() {
                    let socket = TcpSocket::new(self.iface.clone(), tcp_socket);
                    if proxy_handle.is_empty() {
                        proxy_handle.push(self.proxy(socket))
                    }
                }
                self.iface.lock().unwrap().device_mut().put_packet(packet);
            }
            self.iface.lock().unwrap().device_mut().done_inspecting();

            let timestamp = smoltcp::time::Instant::now();

            // The second poll we do process packets in the receive queue from the first poll.
            self.iface_poll(timestamp);

            let poll_delay = self
                .iface
                .lock()
                .unwrap()
                .poll_delay(timestamp)
                .map(|x| Either::Left(tokio::time::sleep(std::time::Duration::from(x))))
                .unwrap_or_else(|| Either::Right(futures::future::pending::<()>()));

            if let Some(receiver) = exit_receiver.as_mut() {
                tokio::select! {
                    _ = poll_delay => {
                        // poll delay elapsed
                    }
                    res = self.pollable_adev.next() => {
                        res.expect("asyncdevice error").expect("what");
                        // asyncdevice did something
                    }
                    _ = receiver.recv() => {
                        debug!("force closing: dns");
                        dns_join_handle.abort();
                        for handle in &proxy_handle {
                            debug!("force closing: proxy");
                            handle.abort()
                        }
                        // FIXME: we need to close arti as well
                        return;
                    }
                }
            } else {
                tokio::select! {
                    _ = poll_delay => {
                        // poll delay elapsed
                    }
                    res = self.pollable_adev.next() => {
                        res.expect("asyncdevice error").expect("what");
                        // asyncdevice did something
                    }
                }
            }
        }
    }
}
