# Onionmasq - Masquerading Onions

This project is an attempt to implement a simple user-space network stack that
can handle TCP and UDP state such that it is possible to forward the traffic
into the Tor network.

## Support

We are hanging out on `#tor-vpn` on the IRC network OFTC. You can also reach this channel via Matrix using `#tor-vpn:matrix.org`.

## Android Development

1. Download [Android Studio](https://developer.android.com/studio) and install
   it. Once it's installed, open it, and follow the setup wizard. Once you are
   in the development environment, create an empty project and then go to
   `Tools` then `SDK Manager`. Here you need to select `NDK (side-by-side)`,
   `Android SDK Platform-Tools`, `Android SDK Build-Tools`, and `Android SDK
   Command-line Tools`. Go get a cup of tea.

2. Please follow the [instructions](https://rustup.rs/) to get the `rustup` tool
   for managing your Rust toolchains -- you will need multiple toolchains, so it
   is unlikely you can use your distribution provided Rust packages.

3. Once you have `rustup` available, please make sure you have the arm64, and
   arm32 Android targets available using:

    $ rustup target add aarch64-linux-android armv7-linux-androideabi

4. Install `cargo-ndk` using

    $ cargo install cargo-ndk

5. Set the environment variables necessary to find the Android SDK/NDK bits
    (note, your ndk version may be different from the example below):
    `export ANDROID_HOME=~/Android/Sdk`
    `export ANDROID_NDK_HOME=~/Android/Sdk/ndk/25.2.9519653/`

    (alternatively, place these in your shell environment initialization files)

5. Try to build onionmasq using

    $ cargo ndk --target arm64-v8a --target armeabi-v7a build --release

## Running

Create the `onion0` interface using (remember to replace `ahf` with
your own username):

    $ sudo ip tuntap add name onion0 mode tun user ahf
    $ sudo ip link set onion0 up
    $ sudo ip addr add 10.42.42.1/24 dev onion0

To route traffic to a specific IP:

    $ ip route add <IP>/32 via 10.42.42.2 dev onion0

To remove it:

    $ ip route del <IP>/32 via 10.42.42.2 dev onion0

## Resources

For further information about Arti and Android, please see the [Arti documentation](https://gitlab.torproject.org/tpo/core/arti/-/blob/main/doc/Android.md)
